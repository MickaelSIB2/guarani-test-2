<?php
/**
 * Created by PhpStorm.
 * User: Mickaël
 * Date: 26/03/2019
 * Time: 18:09
 */

namespace App\Model\Calculator;


interface CalculatorInterface
{
    /**
     * @param int $difference
     * @return array What's given back as an array, with different keys matching the values of the bills
     * (eg array('bill-10' => 1, 'bill-5' => 2...))
     */
    public function getChange(int $difference) : array;
}