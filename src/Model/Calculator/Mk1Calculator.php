<?php
/**
 * Created by PhpStorm.
 * User: Mickaël
 * Date: 26/03/2019
 * Time: 18:11
 */

namespace App\Model\Calculator;

use Exception;

/**
 * MK1 can only have access to 1€ coins
*/
class Mk1Calculator implements CalculatorInterface
{
    // If the change is < than 2, we'll use this function
    public function getChange(int $difference): array {
        return array('coin-1' => $difference);
    }
}