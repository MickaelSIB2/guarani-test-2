<?php
/**
 * Created by PhpStorm.
 * User: Mickaël
 * Date: 26/03/2019
 * Time: 18:34
 */

namespace App\Model\Calculator;


use Exception;

class Mk2Calculator implements CalculatorInterface
{
    private $aChange =  array(
        'coin-1' => 0,
        'coin-2' => 0,
        'bill-5' => 0,
        'bill-10' => 0,
    );

    /**
     * @param int $difference
     * @return array What's given back as an array, with different keys matching the values of the bills
     * (eg array('bill-10' => 1, 'bill-5' => 2...))
     */
    public function getChange(int $difference): array
    {
        // Get the number of bills and coins
        // Get the higher values first then the smaller
        while ($difference > 0) {
            if ($difference >= 10) {
                $this->aChange['bill-10']  += 1;
                $difference -= 10;
            } else if ($difference >= 5) {
                $this->aChange['bill-5']  += 1;
                $difference -= 5;
            } else if ($difference >= 2) {
                $this->aChange['coin-2']  += 1;
                $difference -= 2;
            } else {
                $this->aChange['coin-1']  += 1;
                $difference -= 1;
            }
        }

        return $this->aChange;
    }
}