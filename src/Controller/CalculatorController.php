<?php
/**
 * Created by PhpStorm.
 * User: Mickaël
 * Date: 27/03/2019
 * Time: 18:12
 */

namespace App\Controller;


use App\Registry\CalculatorRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CalculatorController extends Controller
{

    function getChange(int $price, int $amount) : array {
        $difference = $amount - $price;
        if ($amount < $price) {
            throw new Exception('You didn\'t pay enough!');
        }
        $calculator = CalculatorRegistry::getInstance()->getCalculator($difference);

        return $calculator->getChange($difference);
    }


}