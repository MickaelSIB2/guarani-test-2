<?php
/**
 * Created by PhpStorm.
 * User: Mickaël
 * Date: 27/03/2019
 * Time: 17:58
 */

namespace App\Registry;

use App\Model\Calculator\Mk1Calculator;
use App\Model\Calculator\Mk2Calculator;

class CalculatorRegistry implements CalculatorRegistryInterface {
    private static $_calculatorRegistry = false;

    // Get single instance
    public static function getInstance() {
        if (self::$_calculatorRegistry === false) {
            self::$_calculatorRegistry = new CalculatorRegistry();
        }
        return self::$_calculatorRegistry;
    }

    function getCalculator($difference) {
        if ($difference === 1) {
            return new Mk1Calculator();
        } else {
            return new Mk2Calculator();
        }
    }
}